# untitled

拉取项目后：
    记得修改项目下的文件android下的gradle中的gradle-wrapper.properties
修改distributionUrl=https\://services.gradle.org/distributions/gradle-7.2-all.zip


路由跳转：
    在Routes.dart中参照'/appBarDemo':(context)=>AppBarDemoPage()格式添加


app名称修改路径：![img.png](img.png)
  名称：![img_1.png](img_1.png)
  logo：![img_2.png](img_2.png)     (logo图片放在res文件的mipmap文件中的，这里的多个文件是为了适应不同手机的分辨率需要不同尺寸的图片)

查询数据列表
Res res = await Ajax.get('/api/model/table_name',params,sql)
如params中有pageSize或pageNum会返回分页数据，{total:0,rows:[]}

查询单个数据详情
Res res = await Ajax.get('/api/model/table_name',params:{_id:1},sql)

新增数据
Res res = await Ajax.post('/api/model/table_name',data)

修改数据
Res res = await Ajax.put('/api/model/table_name',data:{_id:1,name:'张三'})
data必带_id或id

删除数据
Res res = await Ajax.delete('/api/model/table_name',params,real:false)
real默认false,删除数据都是逻辑删除，数据库中修改该条数据的state值为0,查询列表的时候不会返回
如果需要真正删除请设置real:true
   ```

### 查询参数params说明

```
查询语法类似mongodb, [菜鸟教程](https://www.runoob.com/mongodb/mongodb-query.html)

等于 {key:value}
小于 {key:{$lt:value}}
小于或等于 {key:{$lte:value}}
大于 {key:{$gt:value}}
大于或等于 {key:{$gte:value}}
不等于 {key:{$ne:value}}
AND {key1:value1, key2:value2}
正则 {key:{$regex:value}}
属于 {key:{$in:[value1,value2]}}
```

### 查询结果整理sql说明

```
projection 可选，使用投影操作符指定返回的键。查询时返回文档中所有键值， 只需省略该参数即可（默认省略）
举例 {_id:0,nickName:1,name:'realName'}
0:不返回
1:返回
String:按别名返回，类似SQL的AS

limit 指定读取的记录条数,skip跳过指定数量的数据
举例 {limit:10,skip:0}
注意，分页查询的时候框架会自动计算这两个值

sort 排序
举例: { level: 1, sort: -1 }
1：升序
-1:降序

join 联表查询,可配置多个关联表
举例
{
join:{
deptId: {
type: 'INNER',
name: 'dept',
key: '_id',
query: {
status: 1
},
projection: {
name: 1,
sort: 1,
url: 1,
parentId: 1
}
}
}
}
其中deptId为关联字段名称
type:关联查询类型，LEFT(默认)/INNER/RIGHT
name:关联表名
key:关联表字段名
query:查询关联表条件
projection:关联表字段投影

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

作者：冷建发
QQ：1977206076
邮箱：1977206076@qq.com
