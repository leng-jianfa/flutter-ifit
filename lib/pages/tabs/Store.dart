import 'package:flutter/material.dart';

import '../SearchWidge1.dart';

class StorePage extends StatefulWidget {
  const StorePage({Key? key}) : super(key: key);

  @override
  State<StorePage> createState() => _StorePageState();
}

class _StorePageState extends State<StorePage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  get search => null;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          color: Color(0xff7DDED7),
          child: Column(
            children: [
              //附近门店及输入框
              Container(
                child: Row(
                  children: [
                    //定位图标
                    Container(
                      margin: EdgeInsets.fromLTRB(20, 30, 0, 0),
                      child: Image.asset('assets/images/附近.png'),
                    ),
                    //附近门店字样
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 30, 0, 0),
                      child: Text(
                        "附近门店",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                    ),
                    //搜索框
                    Container(
                        width: 200,
                        height: 30,
                        margin: EdgeInsets.fromLTRB(5, 30, 0, 0),
                        child: SearchWidget(
                          onEditingComplete: search,
                        )
                        // child: TextField(
                        //   decoration: InputDecoration(
                        //     hintText: "请输入门店或服务名",
                        //     border: OutlineInputBorder()
                        //   ),
                        // ),
                        ),
                  ],
                ),
              ),
              //我的车及定位
              Container(
                child: Row(
                  children: [
                    //保时捷718字样
                    Container(
                      margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                      child: Text(
                        "保时捷 718",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                    ),
                    //下箭头图标
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                      child: Image.asset('assets/images/下箭头.png'),
                    ),
                    //定位图标
                    Container(
                      margin: EdgeInsets.fromLTRB(60, 5, 0, 0),
                      child: Image.asset('assets/images/定位.png'),
                    ),
                    //地址
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                      child: Text(
                        "箭滨二路",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                    ),
                    //下箭头图标
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                      child: Image.asset('assets/images/下箭头.png'),
                    ),
                  ],
                ),
              ),
              //小按钮部件
              Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),

                //圆角的设置
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                      bottomLeft: Radius.zero,
                      bottomRight: Radius.zero,
                    )),
                child: Column(
                  children: [
                    //第一行
                    Container(
                      child: Row(
                        children: [
                          //第一个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                                child: Image.asset('assets/images/store1.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                                child: Text("保养服务"),
                              )
                            ],
                          )),
                          //第二个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Image.asset('assets/images/store2.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                child: Text("轮胎服务"),
                              )
                            ],
                          )),
                          //第三个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Image.asset('assets/images/store3.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                child: Text("美容洗车"),
                              )
                            ],
                          )),
                          //第四个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Image.asset('assets/images/store4.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                child: Text("安装服务"),
                              )
                            ],
                          )),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                      child: Row(
                        children: [
                          //第一个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                                child: Image.asset('assets/images/store5.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                                child: Text("划痕补漆"),
                              )
                            ],
                          )),
                          //第二个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Image.asset('assets/images/store6.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                child: Text("精洗打蜡"),
                              )
                            ],
                          )),
                          //第三个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Image.asset('assets/images/store7.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                child: Text("汽车贴膜"),
                              )
                            ],
                          )),
                          //第四个
                          Container(
                              child: Column(
                            children: [
                              //图片
                              Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsets.fromLTRB(25, 20, 0, 0),
                                child: Image.asset('assets/images/store8.png'),
                              ),
                              //文字
                              Container(
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                child: Text("更多"),
                              )
                            ],
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //广告图片
        Container(
          margin: EdgeInsets.fromLTRB(10, 5, 10, 0),
          child: Image.asset('assets/images/广告.png'),
        ),
        //排序
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //综合排序
              Container(
                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Text(
                  "综合排序",
                  style: TextStyle(
                      // fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Color(0xffD1384A)),
                ),
              ),
              Center(
                // 加载 Flutter 内置图标
                child: Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.red,
                ),
              ),
              //距离优先
              Container(
                margin: EdgeInsets.fromLTRB(60, 0, 0, 0),
                child: Text(
                  "距离优先",
                ),
              ),
              //筛选
              Container(
                margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                child: Text(
                  "筛选",
                ),
              ),
              Center(
                // 加载 Flutter 内置图标
                child: Icon(Icons.short_text),
              ),
            ],
          ),
        ),
        //店家店铺1
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Row(
            children: [
              //商家图片
              Container(
                width: 90,
                height: 90,
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Image.asset('assets/images/商家图片.png'),
              ),
              //商家信息
              Container(
                child: Column(
                  children: [
                    //第一行
                    Container(
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: [
                          //合作认证
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),

                                ///圆角
                                border: Border.all(color: Colors.red, width: 1)

                                ///边框颜色、宽
                                ),
                            child: Text(
                              "合作认证",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffD1384A)),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          //店名称
                          Container(
                            child: Text(
                              "金洁快修保洁中心",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    //第二行
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      // alignment: Alignment.bottomLeft,
                      child: Row(
                        children: [
                          //五角星
                          Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.star,
                              color: Colors.red,
                            ),
                          ),
                          //评分
                          Container(
                            child: Text(
                              "4.89",
                              style: TextStyle(color: Colors.red),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 1,
                            height: 10,
                            color: Colors.red,
                          ),
                          //单数
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("2519单"),
                          ),
                          //距离
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              "4.29km",
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              ),
                              // textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
