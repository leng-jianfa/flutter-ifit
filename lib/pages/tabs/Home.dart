import 'package:flutter/material.dart';

import '../SearchWidget.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

//首页
class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  get search => null;

  // 轮播图片
  List<Map> imageList = [
    {"url": "http://www.itying.com/images/flutter/1.png"},
    {"url": "http://www.itying.com/images/flutter/2.png"},
    {"url": "http://www.itying.com/images/flutter/3.png"},
    {"url": "http://www.itying.com/images/flutter/4.png"}
  ];

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        //我的车
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
          // color: Colors.blue,
          child: Row(
            children: [
              //文字部分
              Container(
                child: Column(
                  children: [
                    //车型
                    Container(
                        child: Row(
                      children: [
                        //保时捷718
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 15, 0, 0),
                          child: Text(
                            "保时捷718",
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w500,
                                color: Color(0xff000000)),
                          ),
                        ),
                        //图标
                        Container(
                          height: 10,
                          width: 10,
                          margin: EdgeInsets.fromLTRB(5, 18, 0, 0),
                          child: Image.asset('assets/images/1.png'),
                        )
                      ],
                    )),

                    //添加行驶里程一整行
                    Container(
                        child: Row(
                      children: [
                        //加号图标
                        Container(
                          height: 10,
                          width: 10,
                          margin: EdgeInsets.fromLTRB(12, 15, 0, 15),
                          child: Image.asset('assets/images/2.png'),
                        ),
                        //添加行驶里程免费智能检测
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 15, 0, 15),
                          child: Text(
                            "添加行驶里程免费智能检测",
                            style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w300,
                                color: Color(0xff000000)),
                          ),
                        ),
                      ],
                    ))
                  ],
                ),
              ),
              //汽车图片
              Container(
                height: 80,
                width: 80,
                margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                child: Image.asset('assets/images/car.png'),
              ),
            ],
          ),
        ),
        //搜索框
        Container(
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: SearchWidget(
            onEditingComplete: search,
          ),
        ),
        //小按钮框
        Container(
          child: Column(
            children: [
              //第一行
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,//分散排列
                  children: [
                    //第一个
                    InkWell(
                      child: Container(
                          child: Column(
                        children: [
                          //图片
                          Container(
                            height: 50,
                            width: 50,
                            // margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                            child: Image.asset('assets/images/9.png'),
                          ),
                          //文字
                          Container(
                            // margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                            child: Text("汽车管家"),
                          )
                        ],
                      )),
                      onTap: () {
                        //路由跳转
                        Navigator.pushNamed(context, '/Car_steward');
                      },
                    ),
                    //第二个
                    InkWell(
                      child: Container(
                          child: Column(
                        children: [
                          //图片
                          Container(
                            height: 50,
                            width: 50,
                            // margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                            child: Image.asset('assets/images/6.png'),
                          ),
                          //文字
                          Container(
                            // margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                            child: Text("维保预约"),
                          )
                        ],
                      )),
                      onTap: () {
                        Navigator.pushNamed(context, '/Subscribe');
                      },
                    ),

                    //第三个
                    Container(
                        child: Column(
                      children: [
                        //图片
                        Container(
                          height: 50,
                          width: 50,
                          // margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                          child: Image.asset('assets/images/8.png'),
                        ),
                        //文字
                        Container(
                          // margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: Text("美容洗车"),
                        )
                      ],
                    )),
                    //第四个
                    Container(
                        child: Column(
                      children: [
                        //图片
                        Container(
                          height: 50,
                          width: 50,
                          // margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                          child: Image.asset('assets/images/5.png'),
                        ),
                        //文字
                        Container(
                          // margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: Text("贴膜车衣"),
                        )
                      ],
                    )),
                  ],
                ),
              ),
              //第二行
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //第一个
                    Container(
                        child: Column(
                      children: [
                        //图片
                        Container(
                          height: 50,
                          width: 50,
                          // margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                          child: Image.asset('assets/images/3.png'),
                        ),
                        //文字
                        Container(
                          // margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                          child: Text("轮胎更换"),
                        )
                      ],
                    )),
                    //第二个
                    Container(
                        child: Column(
                      children: [
                        //图片
                        Container(
                          height: 50,
                          width: 50,
                          // margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                          child: Image.asset('assets/images/7.png'),
                        ),
                        //文字
                        Container(
                          // margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: Text("爱车改装"),
                        )
                      ],
                    )),
                    //第三个
                    Container(
                        child: Column(
                      children: [
                        //图片
                        Container(
                          height: 50,
                          width: 50,
                          // margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                          child: Image.asset('assets/images/4.png'),
                        ),
                        //文字
                        Container(
                          // margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: Text("零配件"),
                        )
                      ],
                    )),
                    //第四个
                    Container(
                        child: Column(
                      children: [
                        //图片
                        Container(
                          height: 50,
                          width: 50,
                          // margin: EdgeInsets.fromLTRB(25, 20, 0, 0),
                          child: Image.asset('assets/images/10.png'),
                        ),
                        //文字
                        Container(
                          // margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: Text("更多"),
                        )
                      ],
                    )),
                  ],
                ),
              ),
            ],
          ),
        ),
        //轮播图
        Container(
          //圆角的设置
          // decoration: BoxDecoration(
          //     color: Colors.white,
          //     borderRadius: BorderRadius.only(
          //       topLeft: Radius.circular(20),
          //       topRight: Radius.circular(20),
          //       bottomLeft: Radius.circular(20),
          //       bottomRight: Radius.circular(20),
          //     )
          // ),
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          width: double.infinity,
          child: AspectRatio(
            // 配置宽高比
            aspectRatio: 16 / 9,
            child: new Swiper(
              // itemHeight: 200,
              // itemWidth: 300,
              itemBuilder: (BuildContext context, int index) {
                // 配置图片地址
                return new Image.network(
                  imageList[index]["url"],
                  fit: BoxFit.fill,
                );
              },
              itemWidth: 300,
              itemHeight: 200,
              // 配置图片数量
              itemCount: imageList.length,
              // 底部分页器
              pagination: new SwiperPagination(),
              // 左右箭头
              control: new SwiperControl(),
              // 无限循环
              loop: true,
              // 自动轮播
              autoplay: true,
            ),
          ),
        ),
        //精选推荐字体
        Container(
          alignment: Alignment.center,
          color: Colors.white,
          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
          width: 500,
          child: Text(
            "精选推荐",
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w500,
                color: Color(0xff000000)),
          ),
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
        //商品数据
        Container(
          child: Row(
            children: [
              //商品图片
              Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.fromLTRB(30, 20, 0, 0),
                child: Image.asset('assets/images/11.png'),
              ),
              //商品信息
              Container(
                child: Column(
                  children: [
                    //名称
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 20, 0, 0),
                      child: Text("博世、BOSCH升级版刹车油"),
                    ),
                    //广告语
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text("源自德国 无腐蚀性 制动更灵敏"),
                    ),
                    //价格
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                      child: Text(
                        "￥99",
                        style: TextStyle(color: Colors.red),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        //小灰条
        Container(
          decoration: BoxDecoration(
            color: Colors.black26,
            borderRadius: BorderRadius.all(Radius.circular(3)), //圆角设置
          ),
          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          height: 3,
          alignment: Alignment.center,
        ),
      ],
    );
  }
}
