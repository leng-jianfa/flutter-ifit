import 'package:flutter/material.dart';

class Subscribe extends StatefulWidget {
  const Subscribe({Key? key}) : super(key: key);

  @override
  State<Subscribe> createState() => _SubscribeState();
}

class _SubscribeState extends State<Subscribe>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("维保预约"),
          backgroundColor: Color(0xff6ECCCB),
          elevation: 0, //去掉阴影
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          children: [
            Container(
              color: Color(0xff6ECCCB),
              child: Column(
                children: [
                  //车型   车牌号
                  Container(
                    child: Text(
                      "保时捷 718 " + " " + " 车牌号",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  //车照片
                  Container(
                    width: 300,
                    height: 200,
                    margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15), //圆角
                      // border: Border.all(color: Colors.black12, width: 2) //边框颜色、宽
                    ),
                    child: Image.asset('assets/images/车型.png'),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.black12,
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                    child: Row(
                      children: [
                        Container(
                          width: 2,
                          height: 20,
                          margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          color: Color(0xff6ECCCB),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          child: Text(
                            "预约服务门店",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Spacer(),//填充，使左右对齐
                        Container(
                          child: Text(
                            "更换服务门店",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        InkWell(
                          child: Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.arrow_forward_ios,
                              // color: Colors.red,
                            ),
                          ),
                          onTap: (){

                          },
                        ),
                      ],
                    ),
                  ),
                  Container(),
                ],
              ),
            ),
            Container(),
          ],
        ),
      ),
    );
  }
}
