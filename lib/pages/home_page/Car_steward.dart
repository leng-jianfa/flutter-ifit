import 'package:flutter/material.dart';

class Car_steward extends StatefulWidget {
  const Car_steward({Key? key}) : super(key: key);

  @override
  State<Car_steward> createState() => _Car_stewardState();
}

class _Car_stewardState extends State<Car_steward>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff6ECCCB),
          elevation: 0, //去掉阴影
          title: Text(
            "汽车管家 |" + " 保时捷718",
            style: TextStyle(color: Colors.white),
          ),
          // backgroundColor: Colors.red,
          // centerTitle:true,
          //返回按钮
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),

          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.expand_more),
              onPressed: () {
                print('search');
              },
            ),
            // IconButton(
            //   icon: Icon(Icons.settings),
            //   onPressed: (){
            //     print('settings');
            //   },
            // )
          ],
          // bottom: TabBar(
          //   tabs: <Widget>[
          //     Tab(text: "热门"),
          //     Tab(text: "推荐")
          //   ],
          // ),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              color: Color(0xff6ECCCB),
              child: Row(
                children: [
                  //里程
                  Container(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(20, 50, 0, 0),
                          child: Text(
                            "当前里程",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                          child: Row(
                            children: [
                              Container(
                                child: Text(
                                  "820km  ",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                              InkWell(
                                child: Center(
                                  // 加载 Flutter 内置图标
                                  child: Icon(
                                    Icons.add_to_photos,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  //打败用户
                  Container(
                    margin: EdgeInsets.fromLTRB(40, 50, 0, 0),
                    child: Column(
                      children: [
                        //爱心
                        InkWell(
                          child: Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.heart_broken_sharp,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        //打败了100%的用户  字样
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: Text(
                            "打败了100%的用户",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //保养记录
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), //圆角
                  border: Border.all(color: Colors.black12, width: 2) //边框颜色、宽
                  ),
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Column(
                children: [
                  //添加保养记录
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                            "添加保养记录  ",
                            style: TextStyle(
                              color: Color(0xffE89661),
                              fontSize: 22,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        InkWell(
                          child: Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.chevron_right,
                              color: Color(0xffE89661),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        //圆形图
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                          alignment: Alignment.center,
                          child: ClipOval(
                            child: Image.asset(
                              "assets/images/圆形图.png",
                            ),
                          ),
                        ),

                        //具体内容
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                child: Text(
                                  "下次保养时间/里程：",
                                  style: TextStyle(
                                    // color: Color(0xffE89661),
                                    // fontSize: 22,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                                child: Text(
                                  "2022年12月01日/5000km",
                                  style: TextStyle(
                                    // color: Color(0xffE89661),
                                    // fontSize: 22,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                                child: Text(
                                  "建议保养：",
                                  style: TextStyle(
                                    // color: Color(0xffE89661),
                                    // fontSize: 22,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                                child: Text(
                                  "发动机内部清洗",
                                  style: TextStyle(
                                    // color: Color(0xffE89661),
                                    // fontSize: 22,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 250,
                    height: 45,
                    decoration: BoxDecoration(
                        color: Color(0xff6ECCCB),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(15)) //圆角
                        ),
                    margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
                    alignment: Alignment.center,
                    child: Text(
                      "爱车估值",
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(100, 10, 0, 20),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                            "展开无需处理项",
                            style: TextStyle(
                              // color: Colors.white,
                              // fontSize: 18,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        InkWell(
                          child: Center(
                            // 加载 Flutter 内置图标
                            child: Icon(
                              Icons.expand_more,
                              // color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //套餐推荐
            Container(
              margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text(
                "套餐推荐 :",
                style: TextStyle(
                  // color: Colors.white,
                  // fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            //推荐的商品
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), //圆角
                  border: Border.all(color: Colors.black12, width: 2) //边框颜色、宽
                  ),
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                children: [
                  //商品图片
                  Container(
                    height: 100,
                    width: 100,
                    // margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                    child: Image.asset('assets/images/商品图片.png'),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          child: Text(
                            "驾驰全合成小保养套餐",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "适用粘度（5w-20/5w-30/5w-40）",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              // fontSize: 18,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "套餐官网价：￥199",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              fontWeight: FontWeight.w200,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "小保养" + "￥88",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Text(
                            "升级大保养" + "￥168",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //推荐的商品
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), //圆角
                  border: Border.all(color: Colors.black12, width: 2) //边框颜色、宽
                  ),
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                children: [
                  //商品图片
                  Container(
                    height: 100,
                    width: 100,
                    // margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                    child: Image.asset('assets/images/商品图片.png'),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          child: Text(
                            "驾驰全合成小保养套餐",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "适用粘度（5w-20/5w-30/5w-40）",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              // fontSize: 18,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "套餐官网价：￥199",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              fontWeight: FontWeight.w200,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "小保养" + "￥88",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Text(
                            "升级大保养" + "￥168",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //推荐的商品
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), //圆角
                  border: Border.all(color: Colors.black12, width: 2) //边框颜色、宽
                  ),
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                children: [
                  //商品图片
                  Container(
                    height: 100,
                    width: 100,
                    // margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                    child: Image.asset('assets/images/商品图片.png'),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          child: Text(
                            "驾驰全合成小保养套餐",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "适用粘度（5w-20/5w-30/5w-40）",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              // fontSize: 18,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "套餐官网价：￥199",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              fontWeight: FontWeight.w200,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "小保养" + "￥88",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Text(
                            "升级大保养" + "￥168",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //推荐的商品
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), //圆角
                  border: Border.all(color: Colors.black12, width: 2) //边框颜色、宽
                  ),
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                children: [
                  //商品图片
                  Container(
                    height: 100,
                    width: 100,
                    // margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                    child: Image.asset('assets/images/商品图片.png'),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          child: Text(
                            "驾驰全合成小保养套餐",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "适用粘度（5w-20/5w-30/5w-40）",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              // fontSize: 18,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "套餐官网价：￥199",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              fontWeight: FontWeight.w200,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "小保养" + "￥88",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Text(
                            "升级大保养" + "￥168",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            //推荐的商品
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), //圆角
                  border: Border.all(color: Colors.black12, width: 2) //边框颜色、宽
                  ),
              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                children: [
                  //商品图片
                  Container(
                    height: 100,
                    width: 100,
                    // margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                    child: Image.asset('assets/images/商品图片.png'),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          child: Text(
                            "驾驰全合成小保养套餐",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "适用粘度（5w-20/5w-30/5w-40）",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              // fontSize: 18,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "套餐官网价：￥199",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              fontWeight: FontWeight.w200,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "小保养" + "￥88",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Text(
                            "升级大保养" + "￥168",
                            style: TextStyle(
                              // color: Color(0xffE89661),
                              fontSize: 10,
                              color: Colors.amber,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
