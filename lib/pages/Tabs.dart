
import 'package:flutter/material.dart';
import 'tabs/Home.dart';
import 'tabs/Category.dart';
import 'tabs/Setting.dart';
import 'tabs/Store.dart';
import 'tabs/Mine.dart';

class Tabs extends StatefulWidget {
  final index;
  Tabs({Key? key,this.index=0}) : super(key: key);

  _TabsState createState() => _TabsState(this.index);
}

class _TabsState extends State<Tabs> {

  int _currentIndex;
  _TabsState(this._currentIndex);

  List _pageList=[
    HomePage(),
    CategoryPage(),
    SettingPage(),
    StorePage(),
    MinePage(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color(0xfF2F2F2),
        // appBar: AppBar(
        //   title: Text("Flutter Demo"),
        // ),
        body: this._pageList[this._currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: this._currentIndex,   //配置对应的索引值选中
          onTap: (int index){
              setState(() {  //改变状态
                  this._currentIndex=index;
              });
          },
          iconSize:36.0,      //icon的大小
          fixedColor:Colors.blue,  //选中的颜色
          backgroundColor: Colors.white,
          type:BottomNavigationBarType.fixed,   //配置底部tabs可以有多个按钮
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "首页"
            ),
             BottomNavigationBarItem(
              icon: Icon(Icons.category),
              label: "分类"              
            ),
            
             BottomNavigationBarItem(
              icon: Icon(Icons.public),
              label: "车友圈"
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.location_on),
                label: "附近门店"
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person_add),
                label: "我的"
            )
          ],
        ),
      );
  }
}