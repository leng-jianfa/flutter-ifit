
import 'package:flutter/material.dart';

import '../pages/Tabs.dart';
import '../pages/AppBarDemo.dart';
import '../pages/home_page/Car_steward.dart';
import '../pages/home_page/Subscribe.dart';
import '../pages/tabs/Home.dart';

//配置路由
final Map<String,Function> routes={
      '/':(context)=>Tabs(),
      '/appBarDemo':(context)=>AppBarDemoPage(),
      '/Car_steward':(contrxt)=>Car_steward(),
      '/HomePage':(contrxt)=>HomePage(),
      '/Subscribe':(context)=>Subscribe()
};

//固定写法
var onGenerateRoute=(RouteSettings settings) {
      // 统一处理
      final String? name = settings.name; 
      final Function? pageContentBuilder = routes[name];
      if (pageContentBuilder != null) {
        if (settings.arguments != null) {
          final Route route = MaterialPageRoute(
              builder: (context) =>
                  pageContentBuilder(context, arguments: settings.arguments));
          return route;
        }else{
            final Route route = MaterialPageRoute(
              builder: (context) =>
                  pageContentBuilder(context));
            return route;
        }
      }
};